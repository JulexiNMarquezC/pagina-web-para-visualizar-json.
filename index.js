function datos(){
    const xhttp=new XMLHttpRequest();
    xhttp.open('GET','datos.json',true);
    xhttp.send();
    xhttp.onreadystatechange =function () {
        if(this.readyState==4 && this.status==200){
            let dato=JSON.parse(this.responseText);
            let info=document.querySelector('#datos');
            info.innerHTML="";
            for (let estd of dato) {
                info.innerHTML+=
                ` 
                <tr>
                    <td>
                        ${estd.Cedula}
                    </td>
                    <td>
                        ${estd.Nombre}
                    </td>
                    <td>
                        ${estd.Direccion}
                    </td>
                    <td>
                        ${estd.Telefono}
                    </td>
                    <td>
                        ${estd.Correo}
                    </td>
                    <td>
                        ${estd.Curso}
                    </td>
                    <td>
                        ${estd.Paralelo}
                    </td>
                </tr>  
                `      
            }

        }
    }
}
datos();
